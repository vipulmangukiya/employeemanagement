-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2019 at 11:46 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `solwininfotech`
--

-- --------------------------------------------------------

--
-- Table structure for table `county`
--

CREATE TABLE `county` (
  `c_id` int(11) NOT NULL,
  `c_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `county`
--

INSERT INTO `county` (`c_id`, `c_name`, `created_at`) VALUES
(1, 'India', '2019-08-10 14:48:02'),
(2, 'United States', '2019-08-10 14:53:11'),
(3, 'Canada', '2019-08-10 14:53:11');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `email` varchar(150) NOT NULL,
  `gender` set('0','1') NOT NULL COMMENT ' 0 For Male 1 Female',
  `martial_status` set('0','1') NOT NULL COMMENT '0 for Unmarried 1 for Married',
  `marriage_date` varchar(10) DEFAULT NULL,
  `salary` double NOT NULL,
  `about` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `hobbies_id` varchar(50) NOT NULL,
  `status` set('0','1') NOT NULL COMMENT '0 for Inactive 1 for Acive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `email`, `gender`, `martial_status`, `marriage_date`, `salary`, `about`, `country_id`, `state_id`, `hobbies_id`, `status`, `created_at`) VALUES
(1, 'Vipul', 'mr.vipulmangukiya@gmail.com', '0', '0', '', 450000, 'im self motivated Web Developer', 1, 12, '1,2', '1', '2019-08-11 06:00:46');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `h_id` int(11) NOT NULL,
  `h_name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`h_id`, `h_name`, `created_at`) VALUES
(1, 'Sports', '2019-08-10 14:28:55'),
(2, 'Music ', '2019-08-10 14:28:55'),
(3, 'Traveling', '2019-08-10 14:29:29'),
(4, 'Reading', '2019-08-10 14:29:29'),
(5, 'Gardening', '2019-08-10 14:31:31'),
(6, 'Dancing', '2019-08-10 14:31:31');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `s_id` int(11) NOT NULL,
  `s_c_id` int(11) NOT NULL,
  `s_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`s_id`, `s_c_id`, `s_name`) VALUES
(1, 1, 'Andaman and Nicobar Islands'),
(2, 1, 'Andhra Pradesh'),
(3, 1, 'Arunachal Pradesh'),
(4, 1, 'Assam'),
(5, 1, 'Bihar'),
(6, 1, 'Chandigarh'),
(7, 1, 'Chhattisgarh'),
(8, 1, 'Dadra and Nagar Haveli'),
(9, 1, 'Daman and Diu'),
(10, 1, 'Delhi'),
(11, 1, 'Goa'),
(12, 1, 'Gujarat'),
(13, 1, 'Haryana'),
(14, 1, 'Himachal Pradesh'),
(15, 1, 'Jammu and Kashmir'),
(16, 1, 'Jharkhand'),
(17, 1, 'Karnataka'),
(18, 1, 'Kerala'),
(19, 1, 'Lakshadweep'),
(20, 1, 'Madhya Pradesh'),
(21, 1, 'Maharashtra'),
(22, 1, 'Manipur'),
(23, 1, 'Meghalaya'),
(24, 1, 'Mizoram'),
(25, 1, 'Nagaland'),
(26, 1, 'Orissa'),
(27, 1, 'Pondicherry'),
(28, 1, 'Punjab'),
(29, 1, 'Rajasthan'),
(30, 1, 'Sikkim'),
(31, 1, 'Tamil Nadu'),
(32, 1, 'Tripura'),
(33, 1, 'Uttaranchal'),
(34, 1, 'Uttar Pradesh'),
(35, 1, 'West Bengal'),
(36, 2, 'Alabama'),
(37, 2, 'Alaska'),
(38, 2, 'Arizona'),
(39, 2, 'Arkansas'),
(40, 2, 'California'),
(41, 2, 'Colorado'),
(42, 2, 'Connecticut'),
(43, 2, 'Delaware'),
(44, 2, 'District of Columbia'),
(45, 2, 'Florida'),
(46, 2, 'Georgia'),
(47, 2, 'Hawaii'),
(48, 2, 'Idaho'),
(49, 2, 'Illinois'),
(50, 2, 'Indiana'),
(51, 2, 'Iowa'),
(52, 2, 'Kansas'),
(53, 2, 'Kentucky'),
(54, 2, 'Louisiana'),
(55, 2, 'Maine'),
(56, 2, 'Maryland'),
(57, 2, 'Massachusetts'),
(58, 2, 'Michigan'),
(59, 2, 'Minnesota'),
(60, 2, 'Mississippi'),
(61, 2, 'Missouri'),
(62, 2, 'Montana'),
(63, 2, 'Nebraska'),
(64, 2, 'Nevada'),
(65, 2, 'New Hampshire'),
(66, 2, 'New Jersey'),
(67, 2, 'New Mexico'),
(68, 2, 'New York'),
(69, 2, 'North Carolina'),
(70, 2, 'North Dakota'),
(71, 2, 'Ohio'),
(72, 2, 'Oklahoma'),
(73, 2, 'Oregon'),
(74, 2, 'Pennsylvania'),
(75, 2, 'Rhode Island'),
(76, 2, 'South Carolina'),
(77, 2, 'South Dakota'),
(78, 2, 'Tennessee'),
(79, 2, 'Texas'),
(80, 2, 'Utah'),
(81, 2, 'Vermont'),
(82, 2, 'Virginia'),
(83, 2, 'Washington'),
(84, 2, 'West Virginia'),
(85, 2, 'Wisconsin'),
(86, 2, 'Wyoming'),
(87, 3, 'Alberta'),
(88, 3, 'British Columbia'),
(89, 3, 'Manitoba'),
(90, 3, 'New Brunswick'),
(91, 3, 'Newfoundland and Labrador'),
(92, 3, 'Northwest Territories'),
(93, 3, 'Nova Scotia'),
(94, 3, 'Nunavut'),
(95, 3, 'Ontario'),
(96, 3, 'Prince Edward Island'),
(97, 3, 'Quebec'),
(98, 3, 'Saskatchewan'),
(99, 3, 'Yukon Territory');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `county`
--
ALTER TABLE `county`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`h_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`s_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `county`
--
ALTER TABLE `county`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `h_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
