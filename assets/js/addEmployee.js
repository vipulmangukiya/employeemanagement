(function ($) {
	$.each($.validator.methods, function (key, value) {
		$.validator.methods[key] = function () {           
			if(arguments.length > 0) {
				arguments[0] = $.trim(arguments[0]);
			}
			return value.apply(this, arguments);
		};
	});
} (jQuery));
$(document).ready(function () {
	$(".martial_status").on('change',function(){
		if($(this).val() == 0) {
			$("#marriage_date_cont").addClass('d-none');
		} else {
			$("#marriage_date_cont").removeClass('d-none');

		}
	});
	$("#marriageDate").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: new Date()
	})
	$("#country").on('change',function(){
		var id  = $(this).val();
		if(id!=""){
			$.ajax({
				url: $BASEURL + "CommonData/getState",
				type: "POST",
				data: {id:id},
				dataType: 'json',
				beforeSend:function(){
				},
				success: function (result) {
					if(result.status){
						$html="";
						for (var i = 0; i < result.data.length; i++) {
							$html +='<option value="'+result.data[i].s_id+'">'+result.data[i].s_name+'</option>';
						}
						$("#state").html($html);
					}
				},
				error :function() {
				}
			});
		}
	});
	$("#frmAddEmployee").validate({
		errorElement: 'p',
		rules :{
			name :{required:true},
			country:{required:true},
			email:{required:true,email:true},
			state:{required: true},
			about:{required: true},
			salary: {
				required:true,
				number:true
			},
			status:{required: true},
			'hobbies_id[]':{
				required:true
			},
			marriage_date:{
				required:'#married_chk:checked'
			}
		},
		messages :{
			name: {
				required:"Employee name Can't Blank"
			},
			salary: {
				required:"Salary can't blank",
				number:"Salary only in numeric"
			},
			email:{
				required:"Employee email Can't Blank",
				email:"Please enter valid email"
			},
			country: {
				required:"Select Country"
			},
			state: {
				required:"Select State"
			},
			status: {
				required:"Select Status"
			},
			about: {
				required:"Employee About can't blank"
			},
			'hobbies_id[]' : {
				required: "You Select at least 1 hobbies",
			}
		},
		errorClass:"text-danger", 
		submitHandler: function (form,event) {
			event.preventDefault();
			$.ajax({
				url: $BASEURL + "Employee/add",
				type: "POST",
				data: $("#frmAddEmployee").serialize(),
				dataType: 'json',
				beforeSend:function(){

				},
				success: function (result) {
					if(result.status){
						alert(result.message);
						form.reset();
						setTimeout(function(){
							window.location.reload();
						},250);
					} else {
						alert(result.message);
					}
					 
				},
				error :function() {
					 
				}
			});
		},
		errorPlacement: function (error, element) {
			element.after(error);
		}
	});

});