 
$(document).ready(function () {
	$(".martial_status").on('change',function(){
		if($(this).val() == 0) {
			$("#marriage_date_cont").addClass('d-none');
		} else {
			$("#marriage_date_cont").removeClass('d-none');

		}
	});
	$("#marriageDate").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: new Date()
	})

	$(document).on("click",".deleteEmployee",function(){
		var $that = $(this);
		var id  = $that.attr('data-id');
		if(id!=""){
			$.ajax({
				url: $BASEURL + "employee/delete",
				type: "POST",
				data: {id:id},
				dataType: 'json',
				beforeSend:function(){
				},
				success: function (result) {
					if(result.status){
						alert(result.message);
						$that.parent().parent().remove();
					}
				},
				error :function() {
				}
			});
		}
	});	

	$("#country_id").on('change',function(){
		var id  = $(this).val();
		if(id!=""){
			$.ajax({
				url: $BASEURL + "CommonData/getState",
				type: "POST",
				data: {id:id},
				dataType: 'json',
				beforeSend:function(){
				},
				success: function (result) {
					if(result.status){
						$html="<option value='-1'>All</option>";
						for (var i = 0; i < result.data.length; i++) {
							$html +='<option value="'+result.data[i].s_id+'">'+result.data[i].s_name+'</option>';
						}
						$("#state").html($html);
						if($state!=""){
							$('#state').val($state).change();
						}
					}
				},
				error :function() {
				}
			});
		}
	});
	$("#country_id").trigger('change');

	$(document).on('click','.page-item > a',function(e){
		e.preventDefault();
		$("#frmsearch").attr('action',$(this).attr('href')).submit();

	});

});

$(document).on('click','.sorting', function(){
	var $that  = $(this);
	$sorting =$that.attr('data-value');
	$('.fa-arrow-up').removeClass('active');
	$('.fa-arrow-down').removeClass('active');
	$("#sorting").val($sorting);
	if($that.attr('data-sorting') == 'ASC') {
		$that.find('.fa-arrow-up').addClass('active');
		$that.attr('data-sorting','DESC');
		$("#orderby").val('ASC');
	} else {
		$that.find('.fa-arrow-down').addClass('active');
		$that.attr('data-sorting','ASC');
		$("#orderby").val('DESC');
	}
	$('.sorting').each(function(index, el) {

	});
	saveSate();

	$("#frmsearch").attr('action',window.location.href).submit();
});
function saveSate() {
	$('.sorting').each(function(index, el) {
		localStorage.setItem($(this).attr('data-value'),$(this).attr('data-sorting'));
	});
}
function setSate() {
	$('.sorting').each(function(index, el) {
		if(localStorage.getItem($(this).attr('data-value')) !== null){
			$(this).attr('data-sorting',localStorage.getItem($(this).attr('data-value')));
		}
		if($(this).attr('data-value') == $('#sorting').val()){
			if($('#orderby').val() =='ASC') {
				$(this).find('.fa-arrow-up').addClass('active');
			} else {
				$(this).find('.fa-arrow-down').addClass('active');

			}
		}
	});

}
setSate();


