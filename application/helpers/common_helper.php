<?php 

if (! defined('BASEPATH')) exit('No direct script access allowed');


 if (!function_exists('getCounty')) { 
 	function getCounty() {
 		$CI = & get_instance();
 		return $CI->CommonModel->getCounty();
 	}
 }

 if (!function_exists('getHobbies')) { 
 	function getHobbies() {
 		$CI = & get_instance();
 		return $CI->CommonModel->getHobbies();
 	}
 }

