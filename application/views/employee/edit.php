<script type="text/javascript">
	var $state = "";
	$state = '<?php echo $data['state_id']?>';
</script>
<?php global $pageJSFile;$pageJSFile = 'editEmployee.js'; ?>
<div class="content-middle w-100">
	<div class="container mt-4">
		<div class="row">
			<div class="col-md-3 text-left">
					<h4>Edit Employee Details</h4>
			</div>
			<div class="col-md-5 text-right">
				<a class="btn btn-outline-primary" href="<?php echo base_url('employee')?>">Employee List</a>
				<a class="btn btn-outline-primary" href="<?php echo base_url('employee/add')?>">Add Employee</a>
			</div>
		</div>
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="card">
					<form class="card-body" id="frmEditEmployee" method="post">
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputEmail4">Name</label>
								<input type="text" class="form-control" placeholder="Name" name="name" required="" value="<?php echo $data['name'];?>">
							</div>
							<input type="hidden" name="id" id="editId" value="<?php echo $data['id']?>">
							<div class="form-group col-md-6">
								<label for="inputPassword4">Email</label>
								<input type="email" class="form-control" placeholder="Email" required="" name="email" value="<?php echo $data['email'];?>">
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-group col-md-3">
								<label for="inputPassword4">Gender</label> <br>
								<label class="">
									<input type="radio" name="gender" value="0" autocomplete="off" <?php echo $data['gender']=='0' ? 'checked':'';?>> Male
								</label>
								<label class="">
									<input type="radio" name="gender" value="1" autocomplete="off" <?php echo $data['gender']=='1' ? 'checked':'';?>> Female
								</label>
							</div>
							<div class="form-group col-md-3">
								<label for="inputPassword4">Martial Status</label> <br>
								
								<label class="">
									<input type="radio" class="martial_status" name="martial_status" value="0" autocomplete="off" <?php echo $data['martial_status']=='0' ? 'checked':'';?>> Unmarried
								</label>
								<label class="">
									<input type="radio" id="married_chk" name="martial_status" value="1" autocomplete="off" class="martial_status" <?php echo $data['martial_status']=='1' ? 'checked':'';?> > Married
								</label>
							</div>
							<div class="form-group col-md-3 <?php echo $data['martial_status']=='1' ? 'd-done':'';?>" id="marriage_date_cont">
								<label for="marriageDate">Marriage Date</label>
								<input type="text" name="marriage_date" class="form-control" id="marriageDate" placeholder="Marriage Date" readonly=""  value="<?php echo $data['marriage_date'];?>">
							</div>
							<div class="form-group col-md-3">
								<label for="marriageDate">Salary</label>
								<input type="text" class="form-control" name="salary" placeholder="Salary" value="<?php echo $data['salary'];?>">
							</div>
						</div>
						<div class="form-row ">
							<div class="form-group col-md-3">
								<label for="country">Country</label>
								<select id="country" class="form-control" name="country" required="">
									<option value="">Select Country</option>
									<?php foreach (getCounty() as $key => $value): ?>
										<option value="<?php echo $value['c_id'];?>"  <?php echo $data['country_id']==$value['c_id'] ? 'selected':'';?> ><?php echo $value['c_name'];?></option>
									<?php endforeach ?>

								</select>
							</div>
							<div class="form-group col-md-3">
								<label for="state">State</label>
								<select id="state" class="form-control" name="state">
									<option value="">Select State</option>
								</select>
							</div>
							<div class="form-group col-md-2">
								<label for="status">Status</label>
								<select id="status" class="form-control" name="status">
									<option value="">Select Status</option>
									<option value="1" <?php echo $data['status']=='1' ? 'selected':'';?>>Active</option>
									<option value="0" <?php echo $data['status']=='0' ? 'selected':'';?>>InActive</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="status">Hobbies</label><br>
								<?php foreach (getHobbies() as $key => $value):
									$hobbies = explode(',', $data['hobbies_id']);
									?>
									<div class="form-check form-check-inline">
										<input class="form-check-input" name="hobbies_id[]" type="checkbox" id="inline<?php echo $value['h_id'];?>" value="<?php echo $value['h_id'];?>" <?php echo in_array($value['h_id'],$hobbies) ? 'checked' :'';?>>
										<label class="form-check-label" for="inline<?php echo $value['h_id'];?>"><?php echo $value['h_name'];?></label>
									</div>
								<?php endforeach ?>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputZip">About</label>
								<textarea name="about" class="form-control"><?php echo $data['about'];?></textarea>
							</div>
						</div>
						<div class="text-right">
							
							<button type="submit" class="btn btn-primary text-right">Update Employee Details</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
