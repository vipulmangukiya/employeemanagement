<script type="text/javascript">
	var $state = "";
	$state = '<?php echo isset($_POST['state']) ? $_POST['state']:'-1'?>';
</script>
 
<?php global $pageJSFile;$pageJSFile = 'Employee.js'; ?>
<div class="content-middle w-100">

	<div class="container mt-4">
		
		<div class="row mt-4">
			<div class="col-md-3 text-left">
				<h4>Manage Employee</h4>
			</div>
			<div class="col-md-9 text-right">
				<a class="btn btn-outline-primary" href="<?php echo base_url('employee/add')?>">Add Employee</a>
			</div>
		</div>
		<form action="<?php echo base_url('employee');?> " method="post" class="mt-4" id="frmsearch">
			<div class="row">
				<div class="col-xl-1">
					<label>Per Page</label>
					<div class="form-group">
						<select class="form-control"  name="perPage" id="perPage" >
							<option value="5" <?php if(isset($_POST['perPage']) && $_POST['perPage'] =='5') echo "selected";?>>5</option>
							<option value="10" <?php if(isset($_POST['perPage']) && $_POST['perPage'] =='10') echo "selected"; echo !isset($_POST['perPage']) ? 'selected':'';?>>10</option>
							<option value="15" <?php if(isset($_POST['perPage']) && $_POST['perPage'] =='15') echo "selected";?>>15</option>
							<option value="-1" <?php if(isset($_POST['perPage']) && $_POST['perPage'] =='-1') echo "selected";?>>All</option>
						</select>
					</div>	
				</div>
				<div class="col-xl-2">
					<label> Name</label>
					<div class="form-group">
						<input name="name"  class="form-control" placeholder="Search name" type="text" value="<?php echo isset($_POST['name']) ? $_POST['name'] :'';?>">
					</div>
				</div>

				<div class="col-xl-2">
					<label>Country</label>
					<div class="form-group">
						<select class="form-control"  name="country_id" id="country_id" >
							<option value="-1">Select Country</option>
							<?php foreach (getCounty() as $key => $value): ?>
								<option value="<?php echo $value['c_id']?>" <?php if(isset($_POST['country_id']) && $_POST['country_id'] == $value['c_id']) {echo "selected";}?>><?php echo $value['c_name']?></option>
							<?php endforeach ?>
						</select>
					</div>	
				</div>
				<div class="col-xl-2">
					<label>State</label>
					<div class="form-group">
						<select id="state" class="form-control" name="state">
							<option value="-1">Select State</option>
						</select>
					</div>	
				</div>
				<div class="col-xl-2">
					<label>Status</label>
					<div class="form-group">
						<select id="status" class="form-control" name="status">
							<option <?php if(isset($_POST['status']) && $_POST['status'] =='-1') echo "selected";?> value="-1">All</option>
							<option value="1" <?php if(isset($_POST['status']) && $_POST['status'] =='1') echo "selected";?>>Active</option>
							<option value="0" <?php if(isset($_POST['status']) && $_POST['status'] =='0') echo "selected";?>>InActive</option>
						</select>
					</div>	
				</div>
				<div class="col-xl-2">
					<label>Gender</label>
					<div class="form-group">
						<label class="">
							<input type="radio" name="gender" value="-1" <?php if(isset($_POST['gender']) && $_POST['gender'] =='-1') echo "checked"; echo !isset($_POST['gender']) ? "checked":''; ?>> All
						</label>
						<label class="">
							<input type="radio" name="gender" value="0" <?php if(isset($_POST['gender']) && $_POST['gender'] =='0') echo "checked";?>> Male
						</label>
						<label class="">
							<input type="radio" name="gender" value="1" <?php if(isset($_POST['gender']) && $_POST['gender'] =='1') echo "checked";?>> Female
						</label>
						<input type="hidden" id="sorting" name="sorting" value="<?php echo isset($_POST['sorting']) ? $_POST['sorting']:''?>">
						<input type="hidden" id="orderby" name="orderby" value="<?php echo isset($_POST['orderby']) ? $_POST['orderby']:''?>">
					</div>	
				</div>
				<div class="col-xl-1">
					<button type="submit" class="btn btn-outline-success">Search</button>
				</div>
			</div>
		</form>
		<div class="row align-items-center mt-4">
			<table class="table table-bordered employee_list">
				<thead>
					<tr class="">
						<th scope="col">Employee Id</th>
						<th scope="col" class="w-25 sorting" data-value="name" data-sorting="ASC">Name <i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></th>
						<th scope="col" class="w-25 sorting" data-value="email" data-sorting="ASC">Email <i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></th>
						<th scope="col" class="w-25 sorting" data-value="gender" data-sorting="ASC">Gender<i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></th>
						<th scope="col" class="w-25 sorting" data-value="c_name" data-sorting="ASC">Country <i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></th>
						<th scope="col" class="w-25 sorting" data-value="s_name" data-sorting="ASC">State <i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></th>
						<th scope="col" class="w-25 sorting" data-value="status" data-sorting="ASC">Status <i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></th>
						<th scope="col" class="w-100">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Employees as $key => $value): ?>
						
						<tr>
							<th scope="row"><?php echo $value['id'];?></th>
							<td><?php echo $value['name'];?></td>
							<td><?php echo $value['email'];?></td>
							<td><?php echo $value['gender']=='0' ? 'Male':'Female';?></td>
							<td><?php echo $value['c_name'];?></td>
							<td><?php echo $value['s_name'];?></td>
							<td><?php echo $value['status']=='0' ? 'Inactive':'Active';?></td>
							<td>
								<a class="btn btn-outline-primary btn-sm" href="<?php echo  base_url('employee/edit/'.$value['id'])?>" title="Click To Edit Employee"><i class="fa fa-edit"></i></a>

								<a class="btn btn-outline-danger btn-sm deleteEmployee" href="javaScript:void(0);" title="Click To Edit Delete" onClick="return confirm('are you sure employee ?')" data-id="<?php echo $value['id'];?>"><i class="fa fa-trash"></i></a>
							</td>
							
						</tr>
					<?php endforeach ?>

					<?php if (count($Employees)==0): ?>
						<tr >
							<td colspan="8" class="text-center">No Employee recored </td>
							
						</tr>
					<?php endif ?>

				</tbody>
			</table>
			<p><?php echo $links; ?></p>
		</div>
	</div>
</div>
