<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"/>
	<link rel="stylesheet" href="<?php echo ASSETSURL?>css/custom.css"/>
	<title>Employee Management</title>
	<script type="text/javascript">
		var $BASEURL = '<?php echo BASEURL ?>';
	</script>
</head>
<body>
	<div class="wrapper">
		<div class="container-fluid">