<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

Class CommonModel extends CI_Model {


	function insertData($table,$data) {
		$this->db->trans_start();
		$this->db->insert($table,$data);
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();
		return $insert_id;
	} 

	function updateData($table,$data,$where) {
		$this->db->trans_start();
		$this->db->update($table,$data,$where);
		$this->db->trans_complete();
		return true;
	}

	function deleteData($table,$where) {
		$this->db->trans_start();
		$this->db->delete($table,$where);
		$this->db->trans_complete();
		return true;
	}

	function getCounty($id=null) {
		$this->db->select('*')->from('county');
		$result =  $this->db->get();
		return $result->result_array();
	}

	function getStates($id=null,$country_id=null){
		$this->db->select('*')->from('states');
		
		if($id!==null) {
			$this->db->where('s_id',$id);
		}
		
		if($country_id!==null) {
			$this->db->where('s_c_id',$country_id);
		}
		
		$result =  $this->db->get();
		return $result->result_array();	
	}

	function getHobbies() {
		$this->db->select('*')->from('hobbies');
		$result =  $this->db->get();
		return $result->result_array();
	}

	function getEmployees($employee_id=null,$limit=null, $start=null,$name=null,$country_id=null,$state_id=null,$status=null,$gender=null,$order_col='id',$order_by='desc') {
		$this->db->select('employee.*,county.c_name,states.s_name')->from('employee');
		$this->db->join('county','county.c_id=employee.country_id','left');
		$this->db->join('states','states.s_id=employee.state_id','left');
		if($employee_id!==null) {
			$this->db->where('employee.id',$employee_id);
		}
		if($employee_id!==null) {
			$this->db->where('employee.id',$employee_id);
		}
		if($country_id!==null) {
			$this->db->where('employee.country_id',$country_id);
		}
		if($state_id!==null) {
			$this->db->where('employee.state_id',$state_id);
		}
		if($status!==null) {
			$this->db->where('employee.status',$status);
		}
		if($gender!==null) {
			$this->db->where('employee.gender',$gender);
		}
		if($name!==null) {
			$this->db->where("employee.name LIKE '%$name%'");
		}

		$this->db->order_by($order_col,$order_by);
		if($limit!=null || $start!=null)
			$this->db->limit($limit, $start);
		$result =  $this->db->get();
		return $result->result_array();	
	}

	function checkEmployeeEmail($email){
		$this->db->select('*')->from('employee');
		$this->db->where('email',$email);
		$result =  $this->db->get();
		return $result->result_array();
	}
	function get_count($table) {
		return $this->db->count_all($table);
	}

}