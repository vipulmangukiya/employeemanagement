<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	
	public function index() {

		$perPage=10;
		$name=$country_id=$state=$status=$gender=$sorting=$orderby=null;
		
		if(isset($_POST['perPage']) && $_POST['perPage'] !="-1"){
			$perPage = $_POST['perPage'];
		}
		if(isset($_POST['name']) && $_POST['name'] !=""){
			$name = strip_tags(trim($_POST['name']));
		}
		if(isset($_POST['country_id']) && $_POST['country_id'] !="-1"){
			$country_id = $_POST['country_id'];
		}
		if(isset($_POST['state']) && $_POST['state'] !="-1"){
			$state = $_POST['state'];
		}
		if(isset($_POST['status']) && $_POST['status'] !="-1"){
			$status = $_POST['status'];
		}
		if(isset($_POST['gender']) && $_POST['gender'] !="-1"){
			$gender = $_POST['gender'];
		}
		if(isset($_POST['sorting']) && $_POST['sorting'] !=""){
			$sorting = $_POST['sorting'];
		}
		if(isset($_POST['orderby']) && $_POST['orderby'] !=""){
			$orderby = $_POST['orderby'];
		}
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data['Employees'] = $this->CommonModel->getEmployees(null,null,null,$name,$country_id,$state,$status,$gender,$sorting,$orderby);
		$config = array();
		$config["base_url"] = base_url('employee');
		$config["total_rows"] = count($data['Employees']);
		$config["per_page"] = $perPage;
		$config["uri_segment"] = 2;
		$config['full_tag_open'] = "<nav aria-label='Page navigation example' class='navigation'><ul class='pagination'>";
		$config['full_tag_close'] = '</ul></nav>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="nextpage">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';



		$config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';


		$config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		$data['Employees'] = $this->CommonModel->getEmployees(null,$perPage,$page,$name,$country_id,$state,$status,$gender,$sorting,$orderby);
		$data['main_content'] = 'employee/employee_list';
		$this->load->view('templates/template', $data);
	}

	public function add() {

		if($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->form_validation->set_rules('name', "Employee name Can't Blank", 'trim|required');
			$this->form_validation->set_rules('email', "email", 'trim|required|valid_email|is_unique[employee.email]',array('is_unique' => 'The %s is already exists'));
			$this->form_validation->set_rules('gender', 'Select Gender', 'trim|required');
			$this->form_validation->set_rules('martial_status', 'Martial Status', 'trim|required');
			$this->form_validation->set_rules('salary', "Salary can't blank", 'trim|required');
			$this->form_validation->set_rules('country', 'Select Country ', 'trim|required');
			$this->form_validation->set_rules('state', 'Select State', 'trim|required');
			$this->form_validation->set_rules('status', 'Select Status', 'trim|required');
			$this->form_validation->set_rules('hobbies_id[]', 'Select Hobbies', 'trim|required');
			$this->form_validation->set_rules('about', 'Please enter about', 'trim|required');
			if($this->input->post('martial_status') == 1) {
				$this->form_validation->set_rules('marriage_date', 'Please enter Marriage Date', 'trim|required');
			}

			if($this->form_validation->run() === TRUE) {
				$data_to_add = array(
					"name"			=>$this->input->post('name'),
					"email"			=>$this->input->post('email'),
					"gender"		=>$this->input->post('gender'),
					"martial_status"=>$this->input->post('martial_status'),
					"salary"		=>$this->input->post('salary'),
					"country_id"	=>$this->input->post('country'),
					"state_id"		=>$this->input->post('state'),
					"status"		=>$this->input->post('status'),
					"about"			=>$this->input->post('about'),
					"marriage_date"	=>$this->input->post('marriage_date'),
					"hobbies_id"	=>implode(',', $this->input->post('hobbies_id')),
					"created_at"	=>date('Y-m-d H:s:i')
				);
				$this->CommonModel->insertData('employee',$data_to_add);
				echo json_encode(array('status'=>true,'message'=>'Employee Added Successfully'));	
			} else {
				$msg = str_replace('</p>', '',str_replace('<p>', '', validation_errors()));
				echo json_encode(array('status'=>false,'message'=>$msg));	
			}
			exit();
		}
		$data['main_content'] = 'employee/add';
		$this->load->view('templates/template', $data);	
	}
	public function edit($id) {
		if($id==""){redirect(base_url('employee'));}
		if($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->form_validation->set_rules('name', "Employee name Can't Blank", 'trim|required');
			$this->form_validation->set_rules('email', "email", 'trim|required|valid_email');
			$this->form_validation->set_rules('gender', 'Select Gender', 'trim|required');
			$this->form_validation->set_rules('martial_status', 'Martial Status', 'trim|required');
			$this->form_validation->set_rules('salary', "Salary can't blank", 'trim|required');
			$this->form_validation->set_rules('country', 'Select Country ', 'trim|required');
			$this->form_validation->set_rules('state', 'Select State', 'trim|required');
			$this->form_validation->set_rules('status', 'Select Status', 'trim|required');
			$this->form_validation->set_rules('hobbies_id[]', 'Select Hobbies', 'trim|required');
			$this->form_validation->set_rules('about', 'Please enter about', 'trim|required');
			if($this->input->post('martial_status') == 1) {
				$this->form_validation->set_rules('marriage_date', 'Please enter Marriage Date', 'trim|required');
			}

			if($this->form_validation->run() === TRUE) {
				$res = $this->CommonModel->checkEmployeeEmail($this->input->post('email'));
				if(count($res)==1) {
					if($res[0]['id'] !=$id) {
						echo json_encode(array('status'=>false,'message'=>'Email already exists'));
						exit();
					}
				}
				$data_to_update = array(
					"name"			=>$this->input->post('name'),
					"email"			=>$this->input->post('email'),
					"gender"		=>$this->input->post('gender'),
					"martial_status"=>$this->input->post('martial_status'),
					"salary"		=>$this->input->post('salary'),
					"country_id"	=>$this->input->post('country'),
					"state_id"		=>$this->input->post('state'),
					"status"		=>$this->input->post('status'),
					"about"			=>$this->input->post('about'),
					"marriage_date"	=>$this->input->post('marriage_date'),
					"hobbies_id"	=>implode(',', $this->input->post('hobbies_id'))
				);
				$this->CommonModel->updateData('employee',$data_to_update,array('id'=>$id));
				echo json_encode(array('status'=>true,'message'=>'Employee Updated Successfully'));	
			} else {
				$msg = str_replace('</p>', '',str_replace('<p>', '', validation_errors()));
				echo json_encode(array('status'=>false,'message'=>$msg));	
			}
			exit();
		}
		$data['data'] = $this->CommonModel->getEmployees($id,1,0)[0];
		$data['main_content'] = 'employee/edit';
		$this->load->view('templates/template', $data);	
	}
	function delete() {
		if($this->input->server('REQUEST_METHOD') == 'POST') { 
			$this->CommonModel->deleteData('employee',array('id'=>$this->input->post('id')));
			echo json_encode(array('status'=>true,'message'=>'Employee Deleted Successfully'));	
		}
	}
}
