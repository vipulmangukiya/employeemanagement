<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonData extends CI_Controller {

	
	public function getState() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$countyId = $this->input->post('id');
			$data = $this->CommonModel->getStates(null,$countyId);
			echo json_encode(array('status'=>true,'data'=>$data));	
		}	
	}
}
